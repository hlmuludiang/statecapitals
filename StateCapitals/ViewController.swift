//
//  ViewController.swift
//  StateCapitals
//
//  Created by Versatile Systems, Inc on 8/27/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

import UIKit
import GoogleMaps
import Parse


class ViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
  
  @IBOutlet weak var mapViewUSA: GMSMapView!
  @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
  @IBOutlet weak var resetMapBtn: UIButton!
  
  let locationManager: CLLocationManager = CLLocationManager()
  var boundsUSA: GMSCoordinateBounds = GMSCoordinateBounds()
  var rotatedBounds: GMSCoordinateBounds = GMSCoordinateBounds()
  var capitalMarkers: Array<GMSMarker> = Array<GMSMarker>()
  var visibleMarkers: Array<GMSMarker> = Array<GMSMarker>()
  
  let reachability = Reachability.reachabilityForInternetConnection()
  
  let markerZoom: Float = 12
  var currentZoom: Float?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    mapViewUSA.delegate = self
    mapViewUSA.indoorEnabled = false
    mapViewUSA.settings.rotateGestures = false
    
    locationManager.delegate = self
    locationManager.requestWhenInUseAuthorization()
    
    let buttonImage = UIImage(named: "GreenButton")!.resizableImageWithCapInsets(UIEdgeInsetsMake(18, 18, 18, 18))
    let buttonImageHighlighted = UIImage(named: "GreenButtonHighlighted")!.resizableImageWithCapInsets(UIEdgeInsetsMake(18, 18, 18,18))
    resetMapBtn.setBackgroundImage(buttonImage, forState: UIControlState.Normal)
    resetMapBtn.setBackgroundImage(buttonImageHighlighted, forState: UIControlState.Selected)
    resetMapBtn.showsTouchWhenHighlighted = true
    resetMapBtn.alpha = 0.6
    
    if !reachability.isReachable() {
      reachability.startNotifier()
      NSNotificationCenter.defaultCenter().addObserver(self, selector: "reachabilityChanged:", name: ReachabilityChangedNotification, object: reachability)
    }
  }
  
  func reachabilityChanged(note: NSNotification) {
    
    let reachability = note.object as! Reachability
    
    if reachability.isReachable() {
      reachability.stopNotifier()
      NSNotificationCenter.defaultCenter().removeObserver(self)
      getCapitals()
    }
  }
  
  @IBAction func resetMap(sender: UIButton) {
    displayMap()
    visibleMarkers.removeAll(keepCapacity: false)
    sender.hidden = true
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    if reachability.isReachable() {
      getCapitals()
    }
  }
  
  func getCapitals() {
    let capitalQuery = PFQuery(className:"StateCapitals")
    capitalQuery.findObjectsInBackgroundWithBlock { (stateCapitals, error) -> Void in
      if error == nil {
        for capital in stateCapitals! {
          var capitalMarker: GMSMarker = GMSMarker()
          capitalMarker.position = CLLocationCoordinate2DMake((capital as! PFObject)["latitude"] as! Double, (capital as! PFObject)["longitude"] as! Double)
          capitalMarker.title = ((capital as! PFObject)["capital"] as! String) + " - " + ((capital as! PFObject)["state"] as! String)
          capitalMarker.snippet = "Population: " + toString(((capital as! PFObject)["population"] as! Int))
          self.capitalMarkers.append(capitalMarker)
        }
        self.visibleMarkers = self.capitalMarkers
        self.displayMap()
      }
    }
  }
  
  func displayMap() {
    mapViewUSA.clear()
    boundsUSA = GMSCoordinateBounds()
    for marker in capitalMarkers {
      marker.map = mapViewUSA
      self.boundsUSA = self.boundsUSA.includingCoordinate(marker.position)
    }
    mapViewUSA.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(boundsUSA))
    if !resetMapBtn.hidden {
      resetMapBtn.hidden = true
    }
  }
  
  override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
    currentZoom = mapViewUSA.camera.zoom
  }
  
  override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
    var tempBounds: GMSCoordinateBounds = GMSCoordinateBounds()
    mapViewUSA.clear()
    if visibleMarkers.count > 1 {
      for marker in visibleMarkers {
        tempBounds = tempBounds.includingCoordinate(marker.position)
        marker.map = mapViewUSA
      }
      mapViewUSA.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(tempBounds))
    } else {
      visibleMarkers[0].map = mapViewUSA
      mapViewUSA.animateWithCameraUpdate(GMSCameraUpdate.setTarget(visibleMarkers[0].position, zoom: currentZoom!))
    }
  }
  
  
  func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    if status == .AuthorizedWhenInUse {
      locationManager.startUpdatingLocation()
      if locationManager.location != nil {
        setMapViewOptions(true)
      } else {
        setMapViewOptions(false)
      }
    }
  }
  
  func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
    println("Error updating location: \(error)")
  }
  
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    if let location = locations.last as? CLLocation {
      setMapViewOptions(true)
    }
  }
  
  func mapView(mapView: GMSMapView!, willMove gesture: Bool) {
    loadingIndicator.startAnimating()
    if gesture && boundsUSA.valid {
      visibleMarkers.removeAll(keepCapacity: false)
      resetMapBtn.hidden = false
    }
  }
  
  func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
    var tempBounds: GMSCoordinateBounds = GMSCoordinateBounds()
    tempBounds = tempBounds.includingCoordinate(mapViewUSA.projection.visibleRegion().farLeft)
    tempBounds = tempBounds.includingCoordinate(mapViewUSA.projection.visibleRegion().nearRight)
    mapViewUSA.clear()
    
    if visibleMarkers.isEmpty {
      for marker in capitalMarkers {
        if tempBounds.containsCoordinate(marker.position) {
          marker.map = mapViewUSA
          visibleMarkers.append(marker)
        }
      }
    } else {
      for marker in visibleMarkers {
        marker.map = mapViewUSA
        for tempMarker in capitalMarkers {
          if (tempMarker != marker) && tempBounds.containsCoordinate(tempMarker.position) {
            tempMarker.map = mapViewUSA
          }
        }
      }
    }
    loadingIndicator.stopAnimating()
  }
  
  func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
    resetMapBtn.hidden = false
    return false
  }
  
  func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!) {
    if mapView.camera.zoom  > markerZoom {
      mapViewUSA.animateWithCameraUpdate(GMSCameraUpdate.setTarget(marker.position))
    } else {
      mapViewUSA.animateWithCameraUpdate(GMSCameraUpdate.setTarget(marker.position, zoom: markerZoom))
    }
    resetMapBtn.hidden = false
  }
  
  func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
    return false
  }
  
  func setMapViewOptions(settings: Bool){
    mapViewUSA.myLocationEnabled = settings
    mapViewUSA.settings.myLocationButton = settings
    mapViewUSA.settings.compassButton = settings
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
}

